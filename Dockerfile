FROM node:14-alpine

WORKDIR /home/node/app

COPY taranta-dashboard/. .

RUN npm install --no-cache

CMD ["npm", "start"]